#include "gtest/gtest.h"

#include <optional>
#include <sstream>

#include "calculator.h"

// More of data-driven testing to the God of Testing
using ReturnCode = calc::CalculateExpression::ReturnCode;
using ReturnValue = calc::CalculateExpression::ReturnValue;

struct CalcTestInput {
  std::string in;
  ReturnValue res;
  std::string left_data{};
};

const CalcTestInput simpleTestInput[] = {
    // Correct syntax
    {"1+3*2;", 7},                       // simple calc
    {"(3 * -10 * (2 * 2) / 5 );", -24},  // with parantheses
    {"(3 * -10 * (2 + 2) / 5 );", -24},  // parantheses change order
    {"1 + 2*(1 + 4 / (1+1));", 7},       // inner parantheses
    {"-42;", -42},                       // just negative num
    {"1+(2)+3;", 6},                     // just num in parantheses
    {"8+2+(-3);", 7},                    // just num in parantheses (end)

    // Wrong syntax:
    {"1+3 3*2;", {ReturnCode::MissingOperation}},         // two nums in a row
    {"1+*2;", {ReturnCode::NoOperandForOperation}},       // two ops in a row
    {"1+(3*2;", {ReturnCode::UnmatchedParentheses}},      // not paired '('
    {"1+3)*2;", {ReturnCode::UnmatchedParentheses}},      // not paired ')'
    {"1+1()*2;", {ReturnCode::EmptyParentheses}},         // empty parantheses
    {"1+(1*)+2;", {ReturnCode::NoOperandForOperation}},   // wrong parantheses 1
    {"1+(1)2;", {ReturnCode::MissingOperation}},          // wrong parantheses 2
    {"1(1+2)+2;", {ReturnCode::MissingOperation}},        // wrong parantheses 3
    {"1/0;", {ReturnCode::DivisionByZero}},               // division by zero 1
    {"1/(3-3);", {ReturnCode::DivisionByZero}},           // division by zero 2
    {"2000000000+2000000000;", {ReturnCode::Overflow}},   // overflow '+'
    {"-2000000000-2000000000;", {ReturnCode::Overflow}},  // overflow '-'
    {"100000000*100000000;", {ReturnCode::Overflow}},     // overflow '*'
    {"2-220000000022+1;", {ReturnCode::Overflow}},        // overflow on input
    {"1---3;", {ReturnCode::OwnerlessUnaryOperation}},    // ownerless unary
    {";", {ReturnCode::EmptyExpression}},                 // no expression
    {"d;", {ReturnCode::UnknownToken}},                   // unknown token
    {"1+d-2;", {ReturnCode::UnknownToken}},       // unknown token in the middle
    {"+;", {ReturnCode::NoOperandForOperation}},  // operation with end
    {"1", {ReturnCode::GotEOF}},                  // err: num with no end
    {"+", {ReturnCode::GotEOF}},                  // err: operation with no end
    {"", {ReturnCode::GotEOF}}                    // err: empty input
};

const CalcTestInput leftDataTestInput[] = {
    {"1+(3-2) ; zorz", 2, " zorz"},  // normal input
    {"1+3 3*2; zorz", {}, " zorz"},  // two nums in a row
    {"1+3 3*2 zorz", {}, ""},        // incorrect syntax, no end
    {"1+3-2", {}, "2"},              // correct syntax, no end
    {"1+(3*2;zorz;", {}, "zorz;"},   // not paired left parantheses
    {"1+3)*2;zorz;", {}, "zorz;"},   // not paired right parantheses
    {";zorz;", {}, "zorz;"},         // no expression
    {"1;zorz;", {}, "zorz;"},        // num with no end
    {"+;zorz;", {}, "zorz;"},        // operation with end
    {"d;zorz;", {}, "zorz;"},        // unknown token
    {"1+d-2;zorz;", {}, "zorz;"}     // unknown token in the middle
};

class CalculatorTest : public testing::TestWithParam<CalcTestInput> {};
INSTANTIATE_TEST_CASE_P(CalculatorTestSimple, CalculatorTest,
                        testing::ValuesIn(simpleTestInput));

TEST_P(CalculatorTest, simpleInput) {
  std::istringstream ss{GetParam().in};
  calc::Tokenizer tk{ss};
  calc::CalculateExpression calculate_expression{tk};

  calc::CalculateExpression::ReturnValue res = calculate_expression();
  ASSERT_EQ(GetParam().res.code, res.code);
  if (GetParam().res.code == ReturnCode::Success) {
    EXPECT_EQ(GetParam().res.result, res.result);
  }
}

class CalculatorDataLeftTest : public testing::TestWithParam<CalcTestInput> {};
INSTANTIATE_TEST_CASE_P(CalculatorDataLeftTestInst, CalculatorDataLeftTest,
                        testing::ValuesIn(leftDataTestInput));

TEST_P(CalculatorDataLeftTest, leftDataInput) {
  std::stringstream ss{GetParam().in};
  calc::Tokenizer tk{ss};
  calc::CalculateExpression calculate_expression{tk};

  calculate_expression();
  std::string res;
  getline(ss, res);
  ASSERT_EQ(GetParam().left_data, res);
}

TEST(CalculatorTest, eof_recovery) {
  using rcode = calc::CalculateExpression::ReturnCode;

  calc::CalculateExpression::ReturnValue res;
  std::stringstream in;
  calc::Tokenizer tk{in};
  calc::CalculateExpression calculate{tk};

  //   "122-22+(6*2)/-3;
  in << "12";
  res = calculate();
  ASSERT_EQ(res.code, rcode::GotEOF);

  in.clear();
  in << "2-22";
  res = calculate();
  ASSERT_EQ(res.code, rcode::GotEOF);

  in.clear();
  in << "+(";
  res = calculate();
  ASSERT_EQ(res.code, rcode::GotEOF);

  in.clear();
  in << "6*2)/";
  res = calculate();
  ASSERT_EQ(res.code, rcode::GotEOF);

  in.clear();
  in << "-3;";
  res = calculate();
  ASSERT_EQ(res.code, rcode::Success);
  ASSERT_EQ(res.result, 96);
}

TEST(CalculatorTest, error_recovery) {
  using rcode = calc::CalculateExpression::ReturnCode;

  calc::CalculateExpression::ReturnValue res;
  std::stringstream in;
  calc::Tokenizer tk{in};
  calc::CalculateExpression calculate1{tk};

  in << "12";
  res = calculate1();
  ASSERT_EQ(res.code, rcode::GotEOF);

  in.clear();
  in << "2-+22";  // syntax error UnmatchedParentheses
  res = calculate1();
  ASSERT_EQ(res.code, rcode::GotEOF);

  in.clear();
  in << "+(";
  res = calculate1();
  ASSERT_EQ(res.code, rcode::GotEOF);

  in.clear();
  in << "-3; (1 + ";  // part will goes to the next CalculateExpression
  res = calculate1();
  ASSERT_EQ(res.code, rcode::NoOperandForOperation);

  calc::CalculateExpression calculate2{tk};
  in << "12";
  res = calculate2();
  ASSERT_EQ(res.code, rcode::GotEOF);

  in.clear();
  in << "+3;";
  res = calculate2();
  ASSERT_EQ(res.code, rcode::UnmatchedParentheses);
}
