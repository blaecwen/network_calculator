#include "gtest/gtest.h"

#include <sstream>

#include "calculator.h"

// Let's do some data-driven testing
struct TestInput {
  std::string in;
  std::string res;
};
const TestInput TokenizerGroupInput[] = {
    {"1+3;", "#+#;"},                                // simple
    {"-1+-3;", "-#+-#;"},                            // negative num
    {"1+(-3-2);", "#+(-#-#);"},                      // with parantheses
    {"1+(-3-2)-1;", "#+(-#-#)-#;"},                  // minus after parantheses
    {"1;(3-2);", "#;(#-#);"},                        // multiple ends
    {"1+3-3*2+1/2/3*10-42;", "#+#-#*#+#/#/#*#-#;"},  // long
    {" 1 -\n 2 * 2\n; ", "#-#*#;"},                  // with spaces
    {"1 - - 2 * 2 ; ", "#--#*#;"},                   // space between num&minus
    {"1 - a2 % 2;", "#-?#?#;"},                      // with unknown
    {"2-2100000000+1;", "#-#+#;"},                   // no overflow, max digits
    {"2-2200000000+1;", "#-o+#;"},                   // overflow, max digits
    {"2-22200000000222222+1;", "#-o+#;"}             // overflow, >max digits
};

class TokenizerTest : public testing::TestWithParam<TestInput> {
 protected:
  void expect_tokens(calc::Tokenizer& tk, const std::string& expected,
                     int eof_num = 0) {
    int i = 0;
    for (char ch : expected) {
      EXPECT_EQ(ch, tk.next_token())
          << "Iteration: " << i << "\nExpected tokens: " << expected;
      i++;
    }

    char eof = calc::Tokenizer::tok_eof;
    ASSERT_GE(eof_num, 0);
    for (int i = 0; i < eof_num; ++i) {
      EXPECT_EQ(eof, tk.next_token())
          << "EOF number: " << i << "\nExpected tokens: " << expected;
    }
  }
};
INSTANTIATE_TEST_CASE_P(TokenizerGroup, TokenizerTest,
                        testing::ValuesIn(TokenizerGroupInput));

TEST_P(TokenizerTest, tokenize_expr) {
  std::stringstream in{GetParam().in};
  std::stringstream out;

  calc::Tokenizer tk{in};
  for (size_t i = 0; i < GetParam().res.size(); ++i) {
    out << tk.next_token();
  }
  EXPECT_EQ(GetParam().res, out.str());
}

TEST_F(TokenizerTest, end_of_file) {
  std::string str;
  std::stringstream in;
  calc::Tokenizer tk{in};

  in = std::stringstream{"1+2;"};
  expect_tokens(tk, "#+#;", 2);

  // half-read minus (it is still separate token)
  in = std::stringstream{"1+2-"};
  expect_tokens(tk, "#+#-", 2);
  std::getline(in, str);
  EXPECT_EQ("", str);

  // half-read number
  in = std::stringstream{"1+-23"};
  expect_tokens(tk, "#+-", 2);
  std::getline(in, str);
  EXPECT_EQ("23", str);
}

TEST_F(TokenizerTest, eof_recovery) {
  std::string str;
  std::stringstream in;
  calc::Tokenizer tk{in};

  in << "12+3";
  expect_tokens(tk, "#+", 2);
  EXPECT_EQ(12, tk.last_number());

  in.clear();
  in << "21-";
  expect_tokens(tk, "#-", 2);
  EXPECT_EQ(321, tk.last_number());  // part from previous

  in.clear();
  in << "(1*";
  expect_tokens(tk, "(#*", 2);
  EXPECT_EQ(1, tk.last_number());

  in.clear();
  in << "2)/";
  expect_tokens(tk, "#)/", 2);

  in.clear();
  in << "-3";
  expect_tokens(tk, "-", 1);

  in.clear();
  in << "--1;";
  expect_tokens(tk, "#--#;", 2);  // first '#' from previous

  in.clear();
  in << " - 2 ";
  expect_tokens(tk, "-#", 1);

  in.clear();
  in << " -2 -2";
  expect_tokens(tk, "-#-", 1);
}

TEST_F(TokenizerTest, last_number) {
  std::stringstream in{"7+(42-777) + -3"};
  auto numbers = {7, 7, 7, 42, 42, 777, 777, 777, 777};

  calc::Tokenizer tk{in};
  for (auto num : numbers) {
    tk.next_token();
    ASSERT_EQ(num, tk.last_number());
  }
}
