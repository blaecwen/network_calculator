#!/usr/bin/env python3

import sys
import random


def flip(true_probability):
    return True if random.random() < true_probability else False


class Generator:
    """
    Generates expression for networking calculator.

    NOTE: there are no checks for valid input in order to save development time
    """

    _operations = "+-/*"
    _apply_operation = {
        "+": lambda a, b: a+b,
        "-": lambda a, b: a-b,
        "*": lambda a, b: a*b,
        "/": lambda a, b: a//b,
    }

    def __init__(self):
        self.number_probability = 0.9
        self.nesting_decrease_rate = 0.5

        self.min_num = 0
        self.max_num = 100

    def _generate_operand(self, number_of_operations):
        if number_of_operations < 1 or flip(self.number_probability):
            num = random.randint(self.min_num, self.max_num)
            while num == 0:  # fast work-around to not divide by zero
                num = random.randint(self.min_num, self.max_num)
            expr = str(num)
            return expr, 0
        else:
            new_number_of_operations = random.randint(1, number_of_operations)

            old_num_prob = self.number_probability
            self.number_probability = 1 - (1 - self.number_probability) * self.nesting_decrease_rate
            expr = self.generate_expression(new_number_of_operations)
            self.number_probability = old_num_prob
            return "({})".format(expr), new_number_of_operations

    def _generate_operation(self):
        return random.choice(self._operations)

    def generate_expression(self, number_of_operations):
        """
        Generate expression with specified number of operations.

        NOTE: It is not so efficient 'cause of recursion
        """
        if number_of_operations < 1:
            return

        res = ""
        ops_left = number_of_operations
        while ops_left > 0:
            operand, ops_num = self._generate_operand(ops_left-1)
            res += operand
            ops_left -= ops_num

            operation = self._generate_operation()
            res += operation
            ops_left -= 1

        operand, _ = self._generate_operand(1)
        res += operand

        return res


def main():
    if len(sys.argv) != 2:
        print("Error: Invalid arguments")
        print("\tRun: {} <operations_number>".format(sys.argv[0]))
        return

    gen = Generator()
    expression = gen.generate_expression(int(sys.argv[1]))
    print(expression, ";", sep='')

if __name__ == '__main__':
    main()
