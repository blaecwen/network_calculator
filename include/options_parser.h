#ifndef OPTIONS_PARSER_H
#define OPTIONS_PARSER_H

#include <boost/program_options.hpp>

namespace calc {
namespace po = boost::program_options;

const char help_message[] =
    "network_calculator -- just simple networking calculator.\n\n"
    "Usage:\n"
    "    network_calculator [OPTIONS...]\n\n"
    "All options in \"Configuration\" section can also be specified "
    "in config file defined by --config option.\n"
    "Option specified both in config file and command line arguments "
    "will have value specified in command line.\n";

const char version_message[] =
    "Network calculator especially for NetworkOptix, Dmitry Liman, 2018, "
    "version 0.2";

class OptionsParser {
 public:
  OptionsParser();

  bool check_for_generic(int argc, char* argv[]);
  void parse(int argc, char* argv[], po::variables_map& vm);

 private:
  std::string config_file;

  po::options_description generic;  // allowed only in command line
  po::options_description config;   // general configuration

  po::options_description cmdline_options;  // can be specified in cmd line
  po::options_description file_options;     // can be specified in file
};

}  // namespace calc

#endif  // OPTIONS_PARSER_H
