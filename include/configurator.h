#ifndef CONFIGURATOR_H
#define CONFIGURATOR_H

#include "options_parser.h"

namespace calc {

// NOTE: Value validation lacks here, in realworld user inputted values (e.g.
// cmd/config options) must be validated
class Configurator {
 public:
  Configurator(int argc, char *argv[]);

  char expression_delimiter() const;
  int threads_number() const;
  int port() const;

 private:
  po::variables_map vm;
};

}  // namespace calc

#endif  // CONFIGURATOR_H
