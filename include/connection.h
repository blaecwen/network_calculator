#ifndef CONNECTION_H
#define CONNECTION_H

#include <boost/asio.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <optional>

#include "calculator.h"
#include "configurator.h"

namespace calc {
namespace asio = boost::asio;
using boost::asio::ip::tcp;

class MessageHandler {
 public:
  MessageHandler(asio::streambuf& buffer, char delimiter);
  std::string handle_message();

 private:
  std::istream input_stream_;
  Tokenizer tokenizer_;
  std::optional<CalculateExpression> calculate_expression_;
};

class Connection : public boost::enable_shared_from_this<Connection> {
 public:
  // TODO: not required
  static const size_t max_buffer_size{1024 * 1024 * 1024};  // 1 Gb
  using pointer = boost::shared_ptr<Connection>;

  void start();

  static pointer create(asio::io_service& io_service, const Configurator& cfg);
  tcp::socket& socket();

 private:
  static const size_t max_read_size{4096};

  void read_message();
  bool send_answer(std::string answer);
  void handle_message();

  explicit Connection(asio::io_service& io_service, const Configurator& cfg);

  tcp::socket socket_;
  asio::streambuf buffer_;  // TODO: change to normal buf
  MessageHandler handler_;

  tcp::endpoint client_;  // required to log after client has closed connection
};

}  // namespace calc

#endif  // CONNECTION_H
