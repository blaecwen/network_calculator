#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <cassert>
#include <iostream>
#include <stack>

#include <boost/asio/coroutine.hpp>
#include <boost/system/error_code.hpp>

namespace calc {

class Tokenizer {
 public:
  using token_t = char;

  explicit Tokenizer(std::istream& input, char char_tok_end = tok_end);

  token_t next_token();

  // undefined behavior if last_number called before any next_token calls
  int last_number() const;

  static constexpr token_t tok_number = '#';
  static constexpr token_t tok_overflow = 'o';  // Not a real token
  static constexpr token_t tok_unknown = '?';
  static constexpr token_t tok_end = ';';
  static constexpr token_t tok_eof = EOF;

  static constexpr token_t tok_plus = '+';
  static constexpr token_t tok_minus = '-';
  static constexpr token_t tok_multiplication = '*';
  static constexpr token_t tok_division = '/';
  static constexpr token_t tok_left_parentheses = '(';
  static constexpr token_t tok_right_parentheses = ')';

 private:
  bool is_next_number() const;
  void skip_whitespaces();

  char char_tok_end_;

  int last_number_;
  token_t last_token_{tok_unknown};
  std::istream& input_;
};

// Shunting-yard algorithm
// Another approach is to use flex+bison, but it seems as overkill for that task
class CalculateExpression : boost::asio::coroutine {
 public:
  enum class ReturnCode {
    Success = 0,
    GotEOF,
    DivisionByZero,
    EmptyParentheses,
    UnmatchedParentheses,
    NoOperandForOperation,
    UnknownToken,
    MissingOperation,
    EmptyExpression,
    OwnerlessUnaryOperation,
    Overflow,
    OtherError,
  };

  struct ReturnValue {
    ReturnValue() = default;
    ReturnValue(int result) : result(result) {}
    ReturnValue(ReturnCode code) : code(code) {}
    ReturnValue(int result, ReturnCode code) : result(result), code(code) {}
    int result = 0;
    ReturnCode code = ReturnCode::Success;
  };

  explicit CalculateExpression(Tokenizer& tk);

  ReturnValue operator()();

 private:
  // required to distinguish binary and unary minus
  static constexpr Tokenizer::token_t tok_unary_minus{'m'};

  ReturnCode handle_token(Tokenizer::token_t tok);
  ReturnCode handle_syntax_error();
  ReturnCode apply_operation(Tokenizer::token_t op);

  // saved between yields
  ReturnCode last_return_;
  Tokenizer::token_t last_token_;

  Tokenizer::token_t previous_token_{Tokenizer::tok_unknown};
  std::stack<Tokenizer::token_t> operations_;
  std::stack<int> numbers_;

  Tokenizer& tokenizer_;
};

std::string return_code_to_string(CalculateExpression::ReturnCode code);

}  // namespace calc

#endif  // CALCULATOR_H
