#ifndef LOGGER_H
#define LOGGER_H

#define BOOST_LOG_DYN_LINK 1

#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>

// Just fast and simple implementation of logging.
// In the real world will require some extra work

static inline void init_log(boost::log::trivial::severity_level level) {
  boost::log::core::get()->set_filter(boost::log::trivial::severity >= level);
}

#define trace_l() BOOST_LOG_TRIVIAL(trace)
#define debug_l() BOOST_LOG_TRIVIAL(debug)
#define info_l() BOOST_LOG_TRIVIAL(info)
#define warning_l() BOOST_LOG_TRIVIAL(warning)
#define error_l() BOOST_LOG_TRIVIAL(error)
#define fatal_l() BOOST_LOG_TRIVIAL(fatal)

#endif  // LOGGER_H
