#ifndef APPLICATION_H
#define APPLICATION_H

#include <boost/asio.hpp>
#include <boost/thread.hpp>

#include "logger.h"
#include "configurator.h"
#include "server.h"

namespace calc {

namespace asio = boost::asio;

class Application
{
public:
    Application(int argc, char* argv[]);

    void run();

private:
    asio::io_service io_service_;
    boost::thread_group thread_pool_;

    Configurator configurator_;
    Server server;
    asio::signal_set signals_;
};

}

#endif // APPLICATION_H
