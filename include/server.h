#ifndef SERVER_H
#define SERVER_H

#include <boost/asio.hpp>

#include "configurator.h"
#include "connection.h"

namespace calc {
namespace asio = boost::asio;

class Server {
 public:
  Server(asio::io_service& io_service, Configurator cfg);

 private:
  void start_accept();
  void handle_accept(Connection::pointer new_connection,
                     const boost::system::error_code& error);

  tcp::acceptor acceptor_;
  Configurator configurator_;
};

}  // namespace calc

#endif  // SERVER_H
