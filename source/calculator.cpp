#include "calculator.h"

#include <boost/asio/yield.hpp>

#include <stack>

namespace calc {

void int_putback(int num, std::istream &in) {
  in.clear();
  std::string str = std::to_string(num);
  for (int i = str.size(); i > 0; --i) {
    in.putback(str[i - 1]);
  }
}

Tokenizer::Tokenizer(std::istream &input, char char_tok_end)
    : char_tok_end_{char_tok_end}, input_{input} {}

Tokenizer::token_t Tokenizer::next_token() {
  skip_whitespaces();

  int c = input_.peek();
  if (c == EOF) {
    return last_token_ = tok_eof;
  }

  // handle number
  if (is_next_number()) {
    int new_last_number;
    input_ >> new_last_number;

    if (input_.eof()) {
      input_.clear();
      // number can be half-read, so return number to stream and return eof
      int_putback(new_last_number, input_);
      return last_token_ = tok_eof;
    } else if (input_.fail()) {
      input_.clear();
      return tok_overflow;
    }

    last_number_ = new_last_number;
    last_token_ = tok_number;
    return tok_number;
  }

  input_.ignore(1);
  if (c == char_tok_end_) {  // workaround to support another delimiter
    return last_token_ = tok_end;
  }
  switch (c) {
    case tok_minus:
    case tok_plus:
    case tok_multiplication:
    case tok_division:
    case tok_left_parentheses:
    case tok_right_parentheses:
      last_token_ = c;
      break;
    default:
      last_token_ = tok_unknown;
  }
  return last_token_;
}

int Tokenizer::last_number() const { return last_number_; }

bool Tokenizer::is_next_number() const {
  int c = input_.peek();

  // check for positive
  if (std::isdigit(c)) {
    return true;
  }

  return false;
}

void Tokenizer::skip_whitespaces() {
  if (::isspace(char_tok_end_)) {
    auto c = input_.peek();
    while (c != char_tok_end_ && ::isspace(c)) {
      input_.ignore(1);
      c = input_.peek();
    }
  } else {
    input_ >> std::ws;
  }
}

// NOTE: Requires GCC >= 5
CalculateExpression::ReturnCode CalculateExpression::apply_operation(
    Tokenizer::token_t op) {
  if (numbers_.size() < 2) {
    return ReturnCode::NoOperandForOperation;
  }

  int n2 = numbers_.top();
  numbers_.pop();
  int n1 = numbers_.top();
  numbers_.pop();

  int res;
  switch (op) {
    case Tokenizer::tok_plus:
      if (__builtin_sadd_overflow(n1, n2, &res)) return ReturnCode::Overflow;
      break;
    case Tokenizer::tok_minus:
      if (__builtin_ssub_overflow(n1, n2, &res)) return ReturnCode::Overflow;
      break;
    case Tokenizer::tok_multiplication:
      if (__builtin_smul_overflow(n1, n2, &res)) return ReturnCode::Overflow;
      break;
    case Tokenizer::tok_division:
      if (n2 == 0) return ReturnCode::DivisionByZero;
      res = n1 / n2;
      break;
    case tok_unary_minus:
      return ReturnCode::OwnerlessUnaryOperation;
    default:
      assert(false);
      return ReturnCode::OtherError;
  }

  numbers_.push(res);
  return ReturnCode::Success;
}

CalculateExpression::ReturnCode CalculateExpression::handle_token(
    Tokenizer::token_t tok) {
  switch (tok) {
    case Tokenizer::tok_number:
      numbers_.push(tokenizer_.last_number());
      if (!operations_.empty() && operations_.top() == tok_unary_minus) {
        numbers_.top() = -numbers_.top();
        operations_.pop();
      }
      break;
    case Tokenizer::tok_minus:
      if (previous_token_ != Tokenizer::tok_number &&
          previous_token_ != Tokenizer::tok_right_parentheses) {
        operations_.push(tok_unary_minus);
        return ReturnCode::Success;
      }
      [[fallthrough]];
    case Tokenizer::tok_plus:
      while (!operations_.empty() &&
             operations_.top() != Tokenizer::tok_left_parentheses) {
        auto op = operations_.top();
        auto rc = apply_operation(op);
        if (rc != ReturnCode::Success) return rc;  // syntax error
        operations_.pop();
      }
      operations_.push(tok);
      break;

    case Tokenizer::tok_multiplication:
    case Tokenizer::tok_division:
      while (!operations_.empty() &&
             (operations_.top() == Tokenizer::tok_multiplication ||
              operations_.top() == Tokenizer::tok_division)) {
        auto rc = apply_operation(operations_.top());
        if (rc != ReturnCode::Success) return rc;  // syntax error
        operations_.pop();
      }
      operations_.push(tok);
      break;

    case Tokenizer::tok_left_parentheses:
      operations_.push(tok);
      break;

    case Tokenizer::tok_right_parentheses:
      if (!operations_.empty() &&
          previous_token_ == Tokenizer::tok_left_parentheses) {
        return ReturnCode::EmptyParentheses;
      }
      for (;;) {
        if (operations_.empty()) return ReturnCode::UnmatchedParentheses;

        auto op = operations_.top();
        operations_.pop();
        if (op == Tokenizer::tok_left_parentheses) {
          break;
        }

        auto rc = apply_operation(op);
        if (rc != ReturnCode::Success) return rc;  // syntax error
      }
      break;

    case Tokenizer::tok_overflow:
      return ReturnCode::Overflow;
    case Tokenizer::tok_eof:
      return ReturnCode::GotEOF;
    case Tokenizer::tok_unknown:
      return ReturnCode::UnknownToken;
    case Tokenizer::tok_end:  // should not be called with end
    default:
      assert(false);
      return ReturnCode::OtherError;
  }

  return ReturnCode::Success;
}

CalculateExpression::CalculateExpression(Tokenizer &tk) : tokenizer_(tk) {}


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wimplicit-fallthrough"
CalculateExpression::ReturnValue CalculateExpression::operator()() {
  reenter(this) {
    // main parsing and calculating loop
    last_token_ = tokenizer_.next_token();
    while (last_token_ != Tokenizer::tok_end) {
      last_return_ = handle_token(last_token_);

      if (last_return_ == ReturnCode::GotEOF) {
        yield return {0, ReturnCode::GotEOF};
      } else if (last_return_ != ReturnCode::Success) {
        assert(last_token_ != Tokenizer::tok_end);
        while (handle_syntax_error() == ReturnCode::GotEOF) {
          yield return {0, ReturnCode::GotEOF};
        }
        return {0, last_return_};
      }

      // don't count EOF as prev token
      if (last_token_ != Tokenizer::tok_eof) previous_token_ = last_token_;
      last_token_ = tokenizer_.next_token();
    }

    // loop to calculation operations left after parsing
    while (!operations_.empty()) {
      if (operations_.top() == Tokenizer::tok_left_parentheses) {
        return {0, ReturnCode::UnmatchedParentheses};
      }

      auto rc = apply_operation(operations_.top());
      if (rc != ReturnCode::Success) return {0, rc};
      operations_.pop();
    }

    if (numbers_.empty()) return {0, ReturnCode::EmptyExpression};
    if (numbers_.size() > 1) return {0, ReturnCode::MissingOperation};
  }
  return {numbers_.top(), ReturnCode::Success};
}

CalculateExpression::ReturnCode CalculateExpression::handle_syntax_error() {
  // just clean until expression end or eof
  for (;;) {
    Tokenizer::token_t tok = tokenizer_.next_token();
    if (tok == Tokenizer::tok_eof) {
      return ReturnCode::GotEOF;
    } else if (tok == Tokenizer::tok_end) {
      return ReturnCode::Success;
    }
  }
}
#pragma GCC diagnostic pop


std::string return_code_to_string(CalculateExpression::ReturnCode code) {
  switch (code) {
    case CalculateExpression::ReturnCode::Success:
      return "Success";
    case CalculateExpression::ReturnCode::GotEOF:
      return "GotEOF";
    case CalculateExpression::ReturnCode::DivisionByZero:
      return "DivisionByZero";
    case CalculateExpression::ReturnCode::EmptyParentheses:
      return "EmptyParentheses";
    case CalculateExpression::ReturnCode::UnmatchedParentheses:
      return "UnmatchedParentheses";
    case CalculateExpression::ReturnCode::NoOperandForOperation:
      return "NoOperandForOperation";
    case CalculateExpression::ReturnCode::UnknownToken:
      return "UnknownToken";
    case CalculateExpression::ReturnCode::MissingOperation:
      return "MissingOperation";
    case CalculateExpression::ReturnCode::EmptyExpression:
      return "EmptyExpression";
    case CalculateExpression::ReturnCode::OwnerlessUnaryOperation:
      return "OwnerlessUnaryOperation";
    case CalculateExpression::ReturnCode::Overflow:
      return "Overflow";
    case CalculateExpression::ReturnCode::OtherError:
      return "OtherError";
    default:
      assert(false);
      break;
  }
}

}  // namespace calc
