#include "configurator.h"

#include <boost/thread.hpp>

namespace calc {

Configurator::Configurator(int argc, char *argv[]) {
  calc::OptionsParser().parse(argc, argv, vm);
}

char Configurator::expression_delimiter() const {
  return vm["delimiter"].as<char>();
}

int Configurator::port() const { return vm["port"].as<int>(); }

int Configurator::threads_number() const {
  int threads_num = vm["threads-number"].as<int>();
  if (threads_num == 0) {
    return boost::thread::hardware_concurrency();
  }
  return threads_num;
}

}  // namespace calc
