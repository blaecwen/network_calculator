#include "application.h"

calc::Application::Application(int argc, char* argv[])
    : configurator_(argc, argv),
      server(io_service_, configurator_),
      signals_(io_service_, SIGINT, SIGTERM) {
  signals_.async_wait([this](const boost::system::error_code& error,
                             int signal_number) {
    debug_l() << "Delimiter: '" << configurator_.expression_delimiter() << "'";

    if (error) {
      error_l() << "Error handling signal: " << error.message();
    } else {
      info_l() << "Got signal: '" << signal_number << "'; Stopping io_service";
      io_service_.stop();
    }
  });
}

void calc::Application::run() {
  info_l() << "Running in '" << configurator_.threads_number() << "' threads";
  for (int i = 0; i < configurator_.threads_number(); ++i) {
    thread_pool_.create_thread([this]() {
      while (!io_service_.stopped()) {
        try {
          io_service_.run();
        } catch (std::exception& err) {
          error_l() << "Got unhandled error: " << err.what();
        }
      }
    });
  }

  thread_pool_.join_all();
}
