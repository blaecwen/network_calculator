#include "connection.h"

#include <thread>
#include "boost/make_shared.hpp"

#include "logger.h"

namespace calc {

MessageHandler::MessageHandler(asio::streambuf& buffer, char delimiter)
    : input_stream_{&buffer},
      tokenizer_{input_stream_, delimiter},
      calculate_expression_{tokenizer_} {}

std::string MessageHandler::handle_message() {
  input_stream_.clear();  // clear if got eof before
  // can be a little time-consuming
  CalculateExpression::ReturnValue res = (*calculate_expression_)();

  std::string answer;
  if (res.code == CalculateExpression::ReturnCode::Success) {
    answer = std::to_string(res.result) + "\n";
  } else if (res.code == CalculateExpression::ReturnCode::GotEOF) {
    return "";
  } else {
    answer = "<" + return_code_to_string(res.code) + ">\n";
  }

  calculate_expression_.emplace(tokenizer_);
  return answer;
}

void Connection::start() {
  debug_l() << "Got new connection from '"
            << socket().remote_endpoint().address().to_string() << "'";
  client_ = socket().remote_endpoint();

  boost::system::error_code ec;
  if (!socket_.set_option(tcp::no_delay{true}, ec) &&
      !socket_.set_option(boost::asio::socket_base::keep_alive{true}, ec)) {
  }

  // Not critical error, so just log
  if (ec != boost::system::errc::success) {
    error_l() << "Can't set socket option: " << ec.message() << " ("
              << ec.value() << ")";
  }

  read_message();
}

Connection::pointer Connection::create(asio::io_service& io_service,
                                       const Configurator& cfg) {
  return pointer{new Connection(io_service, cfg)};
}

tcp::socket& Connection::socket() { return socket_; }

void Connection::read_message() {
  auto self{shared_from_this()};
  asio::streambuf::mutable_buffers_type read_bufffer =
      buffer_.prepare(max_read_size);

  socket_.async_read_some(
      asio::buffer(read_bufffer),
      [this, self](boost::system::error_code ec, std::size_t length) {
        buffer_.commit(length);

        if (ec == asio::error::eof) {
          debug_l() << "Client from '" << client_.address().to_string()
                    << "' has closed connection";
          return;
        } else if (ec) {
          // NOTE: in real world it is better to check for possible errors and
          // handle them with less rudeness
          error_l() << "Got error while waiting message: " << ec.message()
                    << " (" << ec.value() << "); Closing connection.";
          return;
        }

        debug_l() << "Got message with length " << length << " bytes";
        std::string answer = handler_.handle_message();
        while (!answer.empty()) {
          send_answer(answer);
          answer = handler_.handle_message();
        }
        read_message();
      });
}

bool Connection::send_answer(std::string answer) {
  if (answer.empty()) {
    return true;
  }

  boost::system::error_code ec;
  // NOTE: answer is small enough, so no need for async_write
  asio::write(socket_, asio::buffer(answer, answer.size()), ec);
  if (ec) {
    error_l() << "Got error while sending answer: " << ec.message() << " ("
              << ec.value() << ")";
    return false;
  } else {
    debug_l() << "Sent result '" << answer << "'";
    return true;
  }
}

Connection::Connection(asio::io_service& io_service, const Configurator& cfg)
    : socket_{io_service},
      buffer_{max_buffer_size},
      handler_{buffer_, cfg.expression_delimiter()} {}

}  // namespace calc
