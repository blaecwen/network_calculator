#include "server.h"

#include <boost/bind.hpp>

#include "logger.h"

namespace calc {

Server::Server(boost::asio::io_service &io_service, Configurator cfg)
    : acceptor_(io_service, tcp::endpoint(tcp::v4(), cfg.port())),
      configurator_(cfg) {
  info_l() << "Listening port '" << cfg.port() << "'";
  start_accept();
}

void Server::start_accept() {
  Connection::pointer new_connection =
      Connection::create(acceptor_.get_io_service(), configurator_);

  acceptor_.async_accept(
      new_connection->socket(),
      boost::bind(&Server::handle_accept, this, new_connection,
                  boost::asio::placeholders::error));
}

void Server::handle_accept(Connection::pointer new_connection,
                           const boost::system::error_code &error) {
  if (!error) {
    new_connection->start();
  } else {
    warning_l() << "Error during accepting connections: " << error.message();
  }

  start_accept();
}
}  // namespace calc
