#include "options_parser.h"

#include <fstream>
#include <iostream>
#include <sstream>

namespace calc {

// NOTE: it is better to define constant strings for options
OptionsParser::OptionsParser()
    : generic("Generic options"), config("General configuration") {
  generic.add_options()("version,v", "print version string")(
      "help,h", "print help message")(
      "config,c",
      po::value<std::string>(&this->config_file)->value_name("filename"),
      "name of a configuration file");

  config.add_options()(
      "delimiter,d",
      po::value<char>()->value_name("delim")->default_value('\n'),
      "character separating different expressions")(
      "threads-number,t", po::value<int>()->value_name("N")->default_value(0),
      "number of worker threads to use. If N equals 0 -- hardware number of "
      "thread will be used\n")("port,p",
                               po::value<int>()->required()->value_name("port"),
                               "Port to listen\n");

  cmdline_options.add(generic).add(config);
  file_options.add(config);
}

bool OptionsParser::check_for_generic(int argc, char *argv[]) {
  config_file.clear();

  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv)
                .options(generic)
                .allow_unregistered()
                .run(),
            vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cerr << help_message << "\n";
    std::cerr << cmdline_options << "\n";
    std::cerr << version_message << "\n";
    return true;
  }

  if (vm.count("version")) {
    std::cerr << version_message << "\n";
    return true;
  }

  return false;
}

void OptionsParser::parse(int argc, char *argv[], po::variables_map &vm) {
  config_file.clear();

  // first load only generic options
  po::store(po::command_line_parser(argc, argv)
                .options(generic)
                .allow_unregistered()
                .run(),
            vm);
  po::notify(vm);

  if (config_file.empty()) {
    po::store(
        po::command_line_parser(argc, argv).options(cmdline_options).run(), vm);
    po::notify(vm);
    return;
  }

  std::ifstream ifs(config_file.c_str());
  if (!ifs) {
    throw std::runtime_error("Can not open config file: " + config_file);
  }

  // load all options
  po::store(po::command_line_parser(argc, argv).options(cmdline_options).run(),
            vm);
  po::store(po::parse_config_file(ifs, file_options), vm);
  po::notify(vm);
}

}  // namespace calc
