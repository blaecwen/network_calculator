#include <iostream>

#include "application.h"

int main(int argc, char* argv[]) {
  try {
    if (calc::OptionsParser().check_for_generic(argc, argv))
      return EXIT_SUCCESS;

    init_log(boost::log::trivial::info);
    info_l() << "Network calculator started";

    calc::Application app(argc, argv);
    app.run();

    info_l() << "Network calculator finished";
    return EXIT_SUCCESS;
  } catch (const std::exception& e) {
    fatal_l() << e.what();
  }
  info_l() << "Network calculator finished with errors";
  return EXIT_FAILURE;
}
