cmake_minimum_required(VERSION 3.10)

project(network_calculator)

option(BUILD_TESTS  "for building unit tests" OFF)

set(CMAKE_MODULE_PATH "/usr/local/orglot/share/cmake/Modules")

set(Boost_USE_STATIC_LIBS OFF)
set(Boost_USE_MULTITHREADED ON)
set(Boost_USE_STATIC_RUNTIME OFF)
find_package(Boost REQUIRED COMPONENTS system thread log program_options)

# compiler flags and definitions
set(VERBOSE_COMPILER_FLAGS -W -Wall -Wextra)
add_compile_options(${VERBOSE_COMPILER_FLAGS})

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set (src_dir source)
set(src
    "${src_dir}/application.cpp"
    "${src_dir}/configurator"
    "${src_dir}/options_parser.cpp"
    "${src_dir}/server.cpp"
    "${src_dir}/connection.cpp"
    "${src_dir}/calculator.cpp" )

set (inc_dir include)
set (inc
    "${inc_dir}/application.h"
    "${inc_dir}/configurator.h"
    "${inc_dir}/connection.h"
    "${inc_dir}/logger.h"
    "${inc_dir}/options_parser.h"
    "${inc_dir}/server.h"
    "${inc_dir}/calculator.h" )

include_directories(${Boost_INCLUDE_DIR})

add_library(${PROJECT_NAME}_lib STATIC ${src} ${inc})
target_link_libraries(${PROJECT_NAME}_lib PUBLIC ${Boost_LIBRARIES} pthread)
target_include_directories(${PROJECT_NAME}_lib PUBLIC ${inc_dir})

add_executable(${PROJECT_NAME} "${src_dir}/main.cpp")
target_link_libraries(${PROJECT_NAME} ${PROJECT_NAME}_lib pthread)

if (BUILD_TESTS)
    add_subdirectory(tests)
endif()
